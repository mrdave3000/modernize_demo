import React, { Component } from 'react'

export default class Header extends Component {
  render() { 
    return (
      <div className='Header'>
        <img alt='' src='/images/logo.svg' />
      </div>
    )
  }
} 
